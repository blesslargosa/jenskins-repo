﻿
namespace OrderingTool
{
    public class Product
    {
        public string artikelnummer { get; set; }

        public string ean { get; set; }

        public string artikelname { get; set; }

        public double buyprice { get; set; }

        public double weightfee { get; set; }

        public double tax { get; set; }

        public string pricetype { get; set; }

        public string supplier { get; set; }
    }
}
