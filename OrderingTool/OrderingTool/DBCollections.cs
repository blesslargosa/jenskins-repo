﻿using DN_Classes.Entities;
using DN_Classes.Supplier;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool
{
  public  class DBCollections
    {

      public static string amazon = Properties.Settings.Default.AmazonSingleCalculated.ToString();
      public static string ebay = Properties.Settings.Default.EbaySingleCalculated.ToString();
      public static string grouporders = Properties.Settings.Default.GroupedOrders.ToString();
      public static string reorder = Properties.Settings.Default.ReOrder.ToString();
      //_copy
        public static MongoCollection<CalculatedProfitEntity> Collection = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/CalculatedProfit")
                                                                              .GetServer().GetDatabase("CalculatedProfit")
                                                                              .GetCollection<CalculatedProfitEntity>(amazon);

        public static MongoCollection<CalculatedProfitEntity> EbayCalculatedCollection = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/CalculatedProfit")
                                                                              .GetServer().GetDatabase("CalculatedProfit")
                                                                              .GetCollection<CalculatedProfitEntity>(ebay);


        public static MongoCollection<GroupedItemsEntity> GroupedItems = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/CalculatedProfit")
                                                                            .GetServer().GetDatabase("CalculatedProfit")
                                                                            .GetCollection<GroupedItemsEntity>(grouporders);


        public static MongoCollection<SupplierEntity> SupplierCollection = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/Supplier")
                                                                             .GetServer().GetDatabase("Supplier")
                                                                             .GetCollection<SupplierEntity>("supplier");

        public static MongoCollection<CalculatedProfitEntity> ReOrderCollection = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/CalculatedProfit")
                                                                             .GetServer().GetDatabase("CalculatedProfit")
                                                                             .GetCollection<CalculatedProfitEntity>(reorder);

        public static MongoCollection<JtlEntity> JtlCollection = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/PD_JTL")
                                                                             .GetServer().GetDatabase("PD_JTL")
                                                                             .GetCollection<JtlEntity>("jtl");


      //------------RETURNS BSON-----------------------------------------

        public static MongoCollection<BsonDocument> EbayCollectionBson = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/CalculatedProfit")
                                                                               .GetServer().GetDatabase("CalculatedProfit")
                                                                               .GetCollection<BsonDocument>(ebay);

        public static MongoCollection<BsonDocument> ReOrderCollectionBson = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/CalculatedProfit")
                                                                               .GetServer().GetDatabase("CalculatedProfit")
                                                                               .GetCollection<BsonDocument>(reorder);

        public static MongoCollection<BsonDocument> AmazonCollectionBson = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/CalculatedProfit")
                                                                           .GetServer().GetDatabase("CalculatedProfit")
                                                                           .GetCollection<BsonDocument>(amazon);


      
    }
}
