﻿using DN_Classes.Entities;
using DN_Classes.Products.Price;
using MongoDB.Bson;
using OrderingTool.Interfaces;
using OrderingTool.Interfaces.Classes;
using OrderingTool.QueriesCommands.ReOrders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhereToBuyDLL;

namespace OrderingTool
{
    public class ReOrdering
    {
      
        ReOrderItemsCommands reOrder = new ReOrderItemsCommands();

        AllPlatforms allplatformsActions = null;


        public ReOrdering() {

            Amazon amazon = new Amazon();
            Ebay ebay = new Ebay();
            ReOrdered reordered = new ReOrdered();

            allplatformsActions = new AllPlatforms(amazon, ebay, reordered);
        
        }

        public void ReOrderItemEanOrderId(string ean, DateTime reOrderDate, string note)
        {
            WhereToBuy getCheapestSupplier = new WhereToBuy();
            BuyPriceRow row = new BuyPriceRow(ean);

            try { row = getCheapestSupplier.GetCheapestPriceByEan(ean, ""); }
            catch { }

            ObjectId id = reOrder.ImportReOrderItemByEan(ean,row, reOrderDate);
            reOrder.UpdateReOrderItemByEan(id, row, note, reOrderDate);
        }


        public void ReOrderItemEanOrderId(string ean, string orderId, DateTime reOrderDate, string note)
        {
            ISingleAction platformAction = new ReOrdered();
            var item = ReOrderItem(ean, orderId, reOrderDate, note);
            reOrder.ImportReOrderItem(item);
            platformAction.UpdateReOrderedItem(item.Ean, item.AmazonOrder.AmazonOrderId, reOrderDate, note, item.plus["supplierOrderId"].ToString());

        }

        public CalculatedProfitEntity ReOrderItem(string ean, string orderId, DateTime reOrderDate, string note)
        {
            ISingleAction platformAction = null;

            platformAction = allplatformsActions.GetOrderPlatform(ean, orderId);
            var order = platformAction.GetSpecificOrderByOrderId(ean, orderId);
            string supplierOrderId = platformAction.GetSupplierOrderId(order);
            platformAction.UpdateReOrderedItem(ean, orderId, reOrderDate, note,supplierOrderId);

            return order;
        }

    }
}
