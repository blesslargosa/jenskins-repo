﻿#region Assembly OrderingTool.dll, v1.0.0.0
// D:\Sir Steff\mvc-webtool8\AmazonOrders\AmazonOrders\bin\OrderingTool.dll
#endregion

using System;
using System.Collections.Generic;

namespace OrderingTool.Entities
{
    public class GroupedSupplierOrderIdItems
    {
        public GroupedSupplierOrderIdItems(string supplierOrderId, List<OrderingTool.GroupedItemsEntity> groupedOrders);

        public List<OrderingTool.GroupedItemsEntity> groupedOrders { get; set; }
        public string supplierOrderId { get; set; }
    }
}
