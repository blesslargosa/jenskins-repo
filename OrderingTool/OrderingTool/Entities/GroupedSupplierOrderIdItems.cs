﻿
using OrderingTool;
using System;
using System.Collections.Generic;

namespace OrderingTool.Entities
{
    public class GroupedSupplierOrderIdItems
    {
        public string supplierOrderId { get; set; }

        public List<GroupedItemsEntity> groupedOrders { get; set; }

        public DateTime dateExported { get; set; }

        public GroupedSupplierOrderIdItems(string supplierOrderId, List<GroupedItemsEntity> groupedOrders, DateTime dateExported)
        {
            this.supplierOrderId = supplierOrderId;
            this.groupedOrders = groupedOrders;
            this.dateExported = dateExported;
        }
    }
}
