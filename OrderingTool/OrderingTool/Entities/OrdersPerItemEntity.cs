﻿
using DN_Classes.Entities;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace OrderingTool.Entities
{
    public class OrdersPerItemEntity
    {
        [BsonId]
        public string id { get; set; }

        public List<CalculatedProfitEntity> singleorders { get; set; }

        public int wareHouseStock { get; set; }

        public int packingUnitToOrder { get; set; }
    }
}
