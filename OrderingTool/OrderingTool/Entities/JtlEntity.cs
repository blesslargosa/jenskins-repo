﻿
using MongoDB.Bson;

namespace OrderingTool
{
    public class JtlEntity
    {
        public ObjectId id { get; set; }

        public Product product { get; set; }

        public BsonDocument plus { get; set; }
    }
}
