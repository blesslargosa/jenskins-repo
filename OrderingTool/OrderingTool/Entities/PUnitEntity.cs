﻿// Decompiled with JetBrains decompiler
// Type: OrderingTool.PUnitEntity
// Assembly: OrderingTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 44FB2E5F-4B66-4E92-8AC0-6B1621E3360C
// Assembly location: D:\REPO\mvc-webtool\AmazonOrders\AmazonOrders\bin\OrderingTool.dll

using DN_Classes.Amazon.Orders;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace OrderingTool
{
    [BsonIgnoreExtraElements]
    public class PUnitEntity
    {
        public string suppliername { get; set; }

        public string ean { get; set; }

        public string artnr { get; set; }

        public int itemsPerVe { get; set; }

        public int countOrders { get; set; }

        public int itemsInStock { get; set; }

        public int itemsToOrder { get; set; }

        public int totalVeToOrder { get; set; }

        public bool ordered { get; set; }

        public DateTime orderedDate { get; set; }

        public List<AmazonOrder> amazonOrderID { get; set; }
    }
}
