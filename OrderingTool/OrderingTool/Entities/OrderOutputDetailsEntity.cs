﻿// Decompiled with JetBrains decompiler
// Type: OrderingTool.Entities.OrderOutputDetailsEntity
// Assembly: OrderingTool, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 44FB2E5F-4B66-4E92-8AC0-6B1621E3360C
// Assembly location: D:\REPO\mvc-webtool\AmazonOrders\AmazonOrders\bin\OrderingTool.dll

using OrderingTool;
using System;
using System.Collections.Generic;

namespace OrderingTool.Entities
{
    public class OrderOutputDetailsEntity
    {
        public string supplierName { get; set; }

        public double veTotalPrice { get; set; }

        public List<GroupedItemsEntity> groupedOrders { get; set; }

        public DateTime minPurchasedDate { get; set; }
    }
}
