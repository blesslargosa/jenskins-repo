﻿
using DN_Classes.Entities;
using System.Collections.Generic;

namespace OrderingTool.Entities
{
    public class ItemsFromOtherSupplierEntity
    {
        public string supplierName { get; set; }

        public List<CalculatedProfitEntity> items { get; set; }
    }
}
