﻿
using ProcuctDB;
using ProcuctDB.Baer;
using ProcuctDB.Berk;
using ProcuctDB.Bieco;
using ProcuctDB.Boltze;
using ProcuctDB.BVW;
using ProcuctDB.Clementoni;
using ProcuctDB.FaberCastell;
using ProcuctDB.Fischer;
using ProcuctDB.Gallay;
using ProcuctDB.Gardena;
using ProcuctDB.Goki;
using ProcuctDB.Haba;
using ProcuctDB.Herweck;
using ProcuctDB.Hoffmann;
using ProcuctDB.Jpc;
using ProcuctDB.JTL;
using ProcuctDB.Knv;
using ProcuctDB.Kosmos;
using ProcuctDB.Leuchtturm;
using ProcuctDB.Loqi;
using ProcuctDB.Moleskine;
using ProcuctDB.Moses;
using ProcuctDB.NLG;
using ProcuctDB.NorisSpiele;
using ProcuctDB.Primavera;
using ProcuctDB.Ravensburger;
using ProcuctDB.Remember;
using ProcuctDB.Schmidt;
//using ProcuctDB.SoundOfSpirit;
using ProcuctDB.Studio100;
using ProcuctDB.Suppliers.Jobo;
using ProcuctDB.Suppliers.SheepWorld;
using ProcuctDB.SWW;
using ProcuctDB.VTech;
using ProcuctDB.Wuerth;

namespace OrderingTool.Entities
{
    public class SupplierIProduct
    {
        private IProduct supplierProd;
       
        public IProduct GetSupplierIProduct(string supplier)
        {
            if (supplier == SupplierEnum.berk.ToString())
                this.supplierProd = (IProduct)new BerkEntity();
            if (supplier == SupplierEnum.baer.ToString())
                this.supplierProd = (IProduct)new BaerEntity();
            if (supplier == SupplierEnum.bieco.ToString())
                this.supplierProd = (IProduct)new BiecoEntity();
            if (supplier == SupplierEnum.boltze.ToString())
                this.supplierProd = (IProduct)new BoltzeEntity();
            if (supplier == SupplierEnum.bvw.ToString())
                this.supplierProd = (IProduct)new BVWEntity();
            if (supplier == SupplierEnum.clementoni.ToString())
                this.supplierProd = (IProduct)new ClementoniEntity();
            if (supplier == SupplierEnum.fabercastell.ToString())
                this.supplierProd = (IProduct)new FaberCastellEntity();
            if (supplier == SupplierEnum.jpc.ToString())
                this.supplierProd = (IProduct)new JpcEntity();
            if (supplier == SupplierEnum.fischer.ToString())
                this.supplierProd = (IProduct)new FischerEntity();
            if (supplier == SupplierEnum.fischer.ToString())
                this.supplierProd = (IProduct)new FischerEntity();
            if (supplier == SupplierEnum.gallay.ToString())
                this.supplierProd = (IProduct)new GallayEntity();
            if (supplier == SupplierEnum.gardena.ToString())
                this.supplierProd = (IProduct)new GardenaEntity();
            if (supplier == SupplierEnum.goki.ToString())
                this.supplierProd = (IProduct)new GokiEntity();
            if (supplier == SupplierEnum.haba.ToString())
                this.supplierProd = (IProduct)new HabaEntity();
            if (supplier == SupplierEnum.herweck.ToString())
                this.supplierProd = (IProduct)new HerweckEntity();
            if (supplier == SupplierEnum.hoffmann.ToString())
                this.supplierProd = (IProduct)new HoffmannEntity();
            if (supplier == SupplierEnum.jtl.ToString())
                this.supplierProd = (IProduct)new JTLEntity();
            if (supplier == SupplierEnum.knv.ToString())
                this.supplierProd = (IProduct)new KnvEntity();
            if (supplier == SupplierEnum.kosmos.ToString())
                this.supplierProd = (IProduct)new KosmosEntity();
            if (supplier == SupplierEnum.leuchtturm.ToString())
                this.supplierProd = (IProduct)new LeuchtturmEntity();
            if (supplier == SupplierEnum.libri.ToString())
                this.supplierProd = (IProduct)new LibriEntity();
            if (supplier == SupplierEnum.loqi.ToString())
                this.supplierProd = (IProduct)new LoqiEntity();
            if (supplier == SupplierEnum.moleskine.ToString())
                this.supplierProd = (IProduct)new MoleskineEntity();
            if (supplier == SupplierEnum.moses.ToString())
                this.supplierProd = (IProduct)new MosesEntity();
            if (supplier == SupplierEnum.nlg.ToString())
                this.supplierProd = (IProduct)new NLGEntity();
            if (supplier == SupplierEnum.norisspiele.ToString())
                this.supplierProd = (IProduct)new NorisSpieleEntity();
            if (supplier == SupplierEnum.primavera.ToString())
                this.supplierProd = (IProduct)new PrimaveraEntity();
            if (supplier == SupplierEnum.ravensburger.ToString())
                this.supplierProd = (IProduct)new RavensburgerEntity();
            if (supplier == SupplierEnum.remember.ToString())
                this.supplierProd = (IProduct)new RememberEntity();
            if (supplier == SupplierEnum.schmidt.ToString())
                this.supplierProd = (IProduct)new SchmidtEntity();
            //if (supplier == SupplierEnum.soundofspirit.ToString())
            //    this.supplierProd = (IProduct)new SoundOfSpiritEntity();
            if (supplier == SupplierEnum.studio100.ToString())
                this.supplierProd = (IProduct)new Studio100Entity();
            if (supplier == SupplierEnum.sww.ToString())
                this.supplierProd = (IProduct)new SWWEntity();
            if (supplier == SupplierEnum.vtech.ToString())
                this.supplierProd = (IProduct)new VTechEntity();
            if (supplier == SupplierEnum.wuerth.ToString())
                this.supplierProd = (IProduct)new WuerthEntity();
            if (supplier == SupplierEnum.sheepworld.ToString())
                this.supplierProd = (IProduct)new SheepWorldEntity();
            if (supplier == SupplierEnum.jobo.ToString())
                this.supplierProd = (IProduct)new JoboEntity();
            return this.supplierProd;
        }
    }
}
