﻿
using DN_Classes.Entities;
using MongoDB.Bson.Serialization.Attributes;
using OrderingTool.Entities;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OrderingTool
{
    [BsonIgnoreExtraElements]
    public class GroupedItemsEntity
    {
        public string supplierOrderId { get; set; }

        public string ean { get; set; }

        public string supplier { get; set; }

        public string supplierSku { get; set; }

        public int packingUnitSize { get; set; }

        public double buyPrice { get; set; }

        public double totalPackingUnitsPrice { get; set; }

        public List<CalculatedProfitEntity> singleOrders { get; set; }

        public int packingUnitsToOrderFromSupplier { get; set; }

        public int itemsInStock { get; set; }

        public int numberOfOrderedItems { get; set; }

        public int itemsToOrderFomSupplier { get; set; }

        public DateTime minPurchasedDate { get; set; }

        public string sellerSKU { get; set; }

        public string SellerPrefix { get; set; }

        public string asin { get; set; }

        public int multiplier { get; set; }

        public GroupedItemsEntity(string ean, string supplier, string supplierSku, int countedOrders, int packingUnitSize, double buyPrice, double totalPackingUnits, DateTime minPurchasedDate, List<CalculatedProfitEntity> singleOrders)
        {
            this.ean = ean;
            this.supplier = supplier;
            this.supplierSku = supplierSku;
            this.packingUnitSize = Math.Max(1, packingUnitSize);
            this.buyPrice = buyPrice;
            this.singleOrders = singleOrders;
            this.minPurchasedDate = minPurchasedDate;
            this.itemsInStock = this.GetNumberOfItemsInStock();
            this.numberOfOrderedItems = Enumerable.Count<CalculatedProfitEntity>((IEnumerable<CalculatedProfitEntity>)singleOrders);
            this.itemsToOrderFomSupplier = this.GetNumberOfItemsToOrder();
            this.sellerSKU = Enumerable.First<IGrouping<string, CalculatedProfitEntity>>(Enumerable.GroupBy<CalculatedProfitEntity, string>((IEnumerable<CalculatedProfitEntity>)singleOrders, (Func<CalculatedProfitEntity, string>)(y => y.SellerSku))).Key;
            this.asin = this.ExtractAsin(this.sellerSKU);
            this.multiplier = this.GetMultiplier(this.asin);
            this.packingUnitsToOrderFromSupplier = this.GetPackingUnitsToOrder();
            this.totalPackingUnitsPrice = (double)this.packingUnitsToOrderFromSupplier * buyPrice * (double)packingUnitSize;

        }

        private string ExtractAsin(string sellerSkU)
        {
            try
            {
                return sellerSKU.Split('-')[1];
            }
            catch { }

            return "";
        }


        private int GetMultiplier(string asin)
        {
            try
            {
                SupplierIProduct supplier = new SupplierIProduct();

                var item = supplier.GetSupplierIProduct(this.ean);

                return item.GetMultiplierByAsin(asin);
            }
            catch { }

            return 1;

        }
        private int GetNumberOfItemsInStock()
        {
            OrderToolDB_AddOns orderToolDb = new OrderToolDB_AddOns();
            try
            {
                int num = 0;
                foreach (JtlEntity jtlEntity in orderToolDb.GetStocksFromWarehouse(this.ean))
                    num += jtlEntity.plus["quantity"].AsInt32;
                return num;
            }
            catch
            {
                return 0;
            }
        }

        public int GetNumberOfItemsToOrder()
        {
            return Math.Max(Enumerable.Count<CalculatedProfitEntity>((IEnumerable<CalculatedProfitEntity>)this.singleOrders) - this.itemsInStock, 0);
        }

        public double GetOrderAmount()
        {
            return (double)this.packingUnitSize * this.buyPrice * (double)this.GetNumberOfItemsToOrder();
        }


        //public int GetPackingUnitsToOrder()
        //{

        //    int num = this.itemsToOrderFomSupplier  / this.packingUnitSize;

        //    if (this.itemsToOrderFomSupplier  % this.packingUnitSize > 0)
        //    {
        //        ++num;
        //    }
        //   return num;
        //}


        public int GetPackingUnitsToOrder()
        {

            double totalItemTOOrder = this.itemsToOrderFomSupplier * this.multiplier;
            string numstring = (totalItemTOOrder / this.packingUnitSize).ToString("0.00"); ;

            double num = double.Parse(numstring);

            int totalVe = 0;
            //double ordersve = totalItemTOOrder / this.packingUnitSize;
            if (totalItemTOOrder % this.packingUnitSize > 0)
            {
                totalVe = Convert.ToInt32(Math.Ceiling(num));
            }
            else
            {
                totalVe = Convert.ToInt32(Math.Floor(num));

            }


            return totalVe;




        }



    }
}
