﻿
using DN_Classes.Entities;
using DN_Classes.Supplier;
using OrderingTool.OrderPurchaseFormatting;
using OrderingTool.OrderPurchaseFormatting.ExportingFormats;
using OrderingTool.OrderPurchaseFormatting.FormatIndicators;
using OrderingTool.OrderPurchaseFormatting.FormattingFactory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace OrderingTool
{
    public class ExportFile
    {
        public void CreateFile(string filePath)
        {
            if (File.Exists(filePath))
            {

                File.Delete(filePath);
            }
            File.Create(filePath).Close();
        }

        public void CreateDirectory(string path)
        {
            if (Directory.Exists(path))
                return;
            Directory.CreateDirectory(path);
        }

        private static string CreateFileName(string supplierOrderId, string path, string addon)
        {
            return path + supplierOrderId + addon + ".txt";
        }

        public string WriteCSV(List<GroupedItemsEntity> groupedorders, string dateTick)
        {
            string supplierOrderId = groupedorders.GroupBy(x => x.supplier).First().Key + "_" + dateTick;

            string path = AppDomain.CurrentDomain.BaseDirectory + "OrderFiles\\";

            this.CreateDirectory(path);

            string filename = CreateFileName(supplierOrderId, path, "");

            this.CreateFile(filename);

            string supplierName = groupedorders.First().supplier;

            #region implementations of the order export formatting
            SupplierDB supplierDB = new SupplierDB();
            SupplierEntity supplierEntity = supplierDB.GetSupplier(supplierName);
            IFormatIndicator formatIndicator = new PlusSectionIndicator(supplierEntity.plus);

            IFormatFactory formatFactory = new ExportFormatFactoryBasic(formatIndicator);
            IExportFormat exportFormat = formatFactory.GetExportFormat();

            exportFormat.Prepare(groupedorders, filename);

            ISupplierOrderExport supplierOrderExport = new SupplierOrderExportBasic();
            supplierOrderExport.Write(exportFormat);
            #endregion

            return filename;
        }

        private static void WriteToFile(List<string[]> lineList, string filename, string separator)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (string[] line in lineList)
                stringBuilder.AppendLine(string.Join(separator, line));

            File.AppendAllText(filename, stringBuilder.ToString());
        }

        public string WriteCSVSupplierOrderId(List<GroupedItemsEntity> odersBySUpplierOrderId, string supplierOrderId)
        {
            List<string[]> lineList = new List<string[]>();
            string path = AppDomain.CurrentDomain.BaseDirectory + "OrderFiles\\";
            this.CreateDirectory(path);
            string filename = CreateFileName(supplierOrderId, path, "_H");
            this.CreateFile(filename);
            string separator = "\t";

            string[] firstline = new string[9]
                 {
                  "EAN",
                  "ARTNR",
                  "SELLERSKU",
                  "PACKINGUNITSIZE",
                  "BUYPRICE",
                  "TOTALVEPRICE",
                  "WAREHOUSE STOCKS",
                  "TOTALORDERS",
                  "ORDEREDITEMS"
                };

            lineList.Add(firstline);

            foreach (GroupedItemsEntity groupedItemsEntity in odersBySUpplierOrderId)
            {
               
                    string articleNumber = Enumerable.First<IGrouping<string, CalculatedProfitEntity>>(Enumerable.GroupBy<CalculatedProfitEntity, string>((IEnumerable<CalculatedProfitEntity>)groupedItemsEntity.singleOrders, (Func<CalculatedProfitEntity, string>)(a => a.ArticleNumber))).Key;

                    string[] column = new string[9];
                    column[0] = groupedItemsEntity.ean;
                    column[1] = groupedItemsEntity.singleOrders.First().ArticleNumber;
                    column[2] = groupedItemsEntity.singleOrders.GroupBy(x => x.SellerSku).First().Key;
                    column[3] = groupedItemsEntity.packingUnitSize.ToString();
                    column[4] = groupedItemsEntity.buyPrice.ToString();
                    column[5] = groupedItemsEntity.totalPackingUnitsPrice.ToString();
                    column[6] = groupedItemsEntity.itemsInStock.ToString();
                    column[7] = groupedItemsEntity.numberOfOrderedItems.ToString();
                    column[8] = groupedItemsEntity.packingUnitsToOrderFromSupplier.ToString();
                    lineList.Add(column);
                
            }
            WriteToFile(lineList, filename, separator);

            return filename;
        }
    }
}
