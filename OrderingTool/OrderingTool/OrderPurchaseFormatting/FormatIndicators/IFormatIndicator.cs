﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.OrderPurchaseFormatting.FormatIndicators
{
    public interface IFormatIndicator
    {
        /// <summary>
        /// returns format indicator
        /// </summary>
        /// <returns>int</returns>
        string GetIndicator();
    }
}
