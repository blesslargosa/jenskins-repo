﻿using OrderingTool.OrderPurchaseFormatting.ExportingFormats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.OrderPurchaseFormatting
{
    public class SupplierOrderExportBasic : ISupplierOrderExport
    {
        public void Write(IExportFormat exportFormat)
        {
            if (exportFormat == null)
            {
                throw new ArgumentNullException("IExportFormat must not be null");
            }

            exportFormat.Write();
        }
    }
}
