﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.OrderPurchaseFormatting.ExportingFormats
{
    public class ExportFormatList
    {
        public Dictionary<string, IExportFormat> FormatList = new Dictionary<string, IExportFormat>();

        public ExportFormatList()
        {
            FormatList.Add("Default", new TxtBasic());
            FormatList.Add("EanList", new TxtEansCommaSeparated());
            FormatList.Add("EanStock", new TxtEanStock());
        }

        public IExportFormat GetExportFormat(string indicator)
        {
            return FormatList.Where(x => x.Key == indicator).First().Value;
        }

    }
}
