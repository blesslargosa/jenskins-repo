﻿using DN_Classes.Entities;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.Interfaces
{
    public interface ISingleAction : IAllActions
    {
        // methods intended for Ebay or Amazon 
    
      CalculatedProfitEntity GetSpecificOrderByOrderId(string ean, string orderId);
      void SetCalculatedOrderToOrdered(string OrderId, string supplierOrderId, string ean, string objectid);
      void UpdateCalculatedOrder(ObjectId objectId, string supplier, string dateTick);
      List<ObjectId> Get_Id(string ean, string orderId, string suppliername);
      String GetSupplierOrderId(CalculatedProfitEntity order);
      void UpdateReOrderedItem(string ean, string orderId, DateTime ReOrderDate, string note, string oldsupplierOrderId);
    }
}
