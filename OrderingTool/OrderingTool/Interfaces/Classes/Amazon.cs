﻿using DN_Classes.Entities;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.Interfaces.Classes
{
    public class Amazon : DBCollections, ISingleAction, IEbayAmazonActions
    {

       
        public List<CalculatedProfitEntity> GetCalculatedOrders()
        {
            var amazonOrders = Collection.Find
                (Query.Or
                (
                Query.And(
                Query.NotExists("plus.supplierOrderId"),
                Query.NotExists("plus.excludeFromOrdering"))
                ,
                Query.And(
                Query.NotExists("plus.supplierOrderId"),
                Query.Exists("plus.excludeFromOrdering"),
                Query.EQ("plus.excludeFromOrdering",false))
                )).ToList();
              
            return amazonOrders;
        }

        public List<CalculatedProfitEntity> GetCalculatedOrdersPerSupplier(string suppliername)
        {
            return Collection.Find(Query.EQ("SupplierName", suppliername)).ToList();
        }

        public BsonDocument GetSpecificOrder(string ean, string supplier)
        {
            return AmazonCollectionBson.FindOne(Query.And(Query.EQ("SupplierName", supplier), Query.EQ("Ean", ean)));

        }

        public void SetCalculatedOrderToOrdered(string orderId, string supplierOrderId, string ean, string objectid)
        {
            if (orderId != null)
            {
                Collection.Update(Query.And(Query.EQ("AmazonOrder.AmazonOrderId", orderId), Query.EQ("Ean", ean)), Update.Set("plus.supplierOrderId", supplierOrderId).Set("plus.ordered", true));
            }
            else
            {
                Collection.Update(Query.And(Query.EQ("_id", ObjectId.Parse(objectid)), Query.EQ("Ean", ean)), Update.Set("plus.supplierOrderId", supplierOrderId).Set("plus.ordered", true));
            }
        }

        public void UpdateCalculatedOrder(ObjectId objectId, string supplier, string dateTick)
        {
            
                string obj = objectId.ToString().Replace("{", "").Replace("}", ""); 

                var order = Collection.FindOne(Query.EQ("_id", ObjectId.Parse(obj)));

                if (order.plus == null || !order.plus.Contains("supplierOrderId"))
                {
                    Collection.Update(Query.EQ("_id", ObjectId.Parse(obj)),
                                 Update.Set("plus.supplierOrderId", supplier + "_" + dateTick)
                                 .Set("plus.ordered", true).Set("plus.excludeFromOrdering", true));
                }
            
        }

        public CalculatedProfitEntity GetSpecificOrderByOrderId(string ean, string orderId)
        {
            return Collection.FindOne(Query.And(Query.EQ("Ean", ean), Query.EQ("AmazonOrder.AmazonOrderId", orderId)));
        }

        public string GetEanByOrderId(string orderId)
        {
            
            var orderedItems = Collection.Find(Query.EQ("AmazonOrder.AmazonOrderId", orderId));

            if (orderedItems.Count() == 1)
            {

                return orderedItems.First().Ean;
            }

            return ""; 
        }


        public ISingleAction GetOrderPlatform(string ean, string orderId)
        {
            if (GetSpecificOrderByOrderId(ean,orderId) != null)
            {

                return new Amazon();
            }
            return null;
        }


        public List<CalculatedProfitEntity> GetSameOrders(string ean, string suppliername)
        {
            return Collection.Find(Query.And(Query.EQ("Ean", ean), Query.EQ("SupplierName", suppliername))).ToList();
        }


        public List<string> GetAllOrderIds(GroupedItemsEntity order)
        {
           
            return order.singleOrders.Where(x => x.AmazonOrder != null).Select(x => x.AmazonOrder.AmazonOrderId).ToList();
        }


        public List<ObjectId> Get_Id(string ean, string orderId, string suppliername)
        {
            List<ObjectId> ids = new List<ObjectId>();
            var orders = Collection.Find(Query.And(Query.EQ("Ean", ean), Query.EQ("AmazonOrder.AmazonOrderId", orderId), Query.EQ("SupplierName", suppliername))).ToList();

            orders.ForEach(x => ids.Add(x._id));
            return ids;
        }

        public string GetSupplierOrderId(CalculatedProfitEntity order)
        {
            if (order.plus["supplierOrderId"] != null) {

                return order.plus["supplierOrderId"].ToString();
            
            }

            return "";
        }

        public void UpdateReOrderedItem(string ean, string orderId, DateTime ReOrderDate, string note, string oldsupplierOrderId)
        {
            Collection.Update(Query.And(Query.EQ("Ean", ean), Query.EQ("AmazonOrder.AmazonOrderId", orderId)),
               Update.Set("plus.note", note).
               Push("plus.reOrderDate", ReOrderDate));
        }

        public List<CalculatedProfitEntity> GetCalculatedOrdersByEan(string ean)
        {
            return Collection.Find(Query.EQ("Ean", ean)).ToList();
        }

        public BsonDocument GetSpecificBsonOrder(ObjectId id)
        {
            return AmazonCollectionBson.FindOne(Query.EQ("_id", id));
        }


        public List<string> GetMultipleEansByOrderId(string orderId)
        {
           var orders = Collection.Find(Query.EQ("AmazonOrder.AmazonOrderId", orderId)).ToList();

           return orders.Select(x => x.Ean).ToList();
        }
    }
}
