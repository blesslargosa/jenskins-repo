﻿using AmazonOrders;
using DN_Classes.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.Interfaces.Classes
{
    public class ReOrdered : DBCollections, ISingleAction
    {
        public List<CalculatedProfitEntity> GetCalculatedOrders()
        {
            return ReOrderCollection.Find(Query.Or
                (
                Query.And(
                Query.NotExists("plus.supplierOrderId"),
                Query.NotExists("plus.excludeFromOrdering"))
                ,
                Query.And(
                Query.NotExists("plus.supplierOrderId"),
                Query.Exists("plus.excludeFromOrdering"),
                Query.EQ("plus.excludeFromOrdering", false))
                )).ToList();
        }

        public List<CalculatedProfitEntity> GetCalculatedOrdersPerSupplier(string suppliername)
        {

            return ReOrderCollection.Find(Query.EQ("SupplierName", suppliername)).ToList();
        }

        public List<string> GetAllOrderIds(GroupedItemsEntity order)
        {
            // return  _id's of all items that have no amazon and ebay field
            return order.singleOrders.Where(x => x.EbayOrder == null && x.AmazonOrder == null).Select(x => x._id.ToString()).ToList();
        }

        public CalculatedProfitEntity GetSpecificOrderByOrderId(string ean, string orderId)
        {
            throw new NotImplementedException();
        }

        public void SetCalculatedOrderToOrdered(string OrderId, string supplierOrderId, string ean, string objectid)
        {
            throw new NotImplementedException();
        }

        public void UpdateCalculatedOrder(ObjectId objectId, string supplier, string dateTick)
        {
            string obj = objectId.ToString().Replace("{", "").Replace("}", "");

             var order = ReOrderCollection.FindOne(Query.EQ("_id", ObjectId.Parse(obj)));

            if(order.plus == null || !order.plus.Contains("supplierOrderId")){
            ReOrderCollection.Update(Query.EQ("_id", ObjectId.Parse(obj)),
                         Update.Set("plus.supplierOrderId", supplier + "_" + dateTick)
                         .Set("plus.ordered", true).Set("plus.excludeFromOrdering", true));
            }
        }

        public List<ObjectId> Get_Id(string ean, string orderId,string suppliername)
        {

            List<ObjectId> ids = new List<ObjectId>();
           
            var orders = ReOrderCollection.Find(Query.And(Query.EQ("Ean", ean), Query.EQ("EbayOrder.OrderID", orderId), Query.EQ("SupplierName", suppliername))).ToList();

            if (orders == null || orders.Count() == 0)
            {
                orders = ReOrderCollection.Find(Query.And(Query.EQ("Ean", ean), Query.EQ("AmazonOrder.AmazonOrderId", orderId), Query.EQ("SupplierName", suppliername))).ToList();
            }

            orders.ForEach(x => ids.Add(x._id));
            return ids;
        }

        public BsonDocument GetSpecificOrder(string ean, string supplier)
        {
            return ReOrderCollectionBson.FindOne(Query.And(Query.EQ("SupplierName", supplier), Query.EQ("Ean", ean)));
        }


        public string GetSupplierOrderId(CalculatedProfitEntity order)
        {
            if (order.plus["supplierOrderId"] != null)
            {

                return order.plus["supplierOrderId"].ToString();

            }

            return "";
        }


        public void UpdateReOrderedItem(string ean, string orderId, DateTime ReOrderDate, string note, string oldsupplierOrderId)
        {
            

            ReOrderCollection.Update(Query.And(Query.EQ("Ean", ean), 
                                               Query.EQ("EbayOrder.OrderID", orderId),
                                               Query.EQ("plus.supplierOrderId", oldsupplierOrderId)),
            Update.Set("plus.reOrderDate", ReOrderDate)
            .Set("plus.oldSupplierOrderId", oldsupplierOrderId)
            .Set("plus.note", note)
            .Unset("plus.ordered")
            .Unset("plus.excludeFromOrdering")
            .Unset("plus.supplierOrderId"), UpdateFlags.Multi);

            ReOrderCollection.Update(Query.And( Query.EQ("Ean", ean), 
                                                Query.EQ("AmazonOrder.AmazonOrderId", orderId),
                                                Query.EQ("plus.supplierOrderId", oldsupplierOrderId)),

            Update.Set("plus.reOrderDate", ReOrderDate)
            .Set("plus.oldSupplierOrderId", oldsupplierOrderId)
            .Set("plus.note", note)
            .Unset("plus.ordered")
            .Unset("plus.excludeFromOrdering")
            .Unset("plus.supplierOrderId"), UpdateFlags.Multi);
        }


        public List<CalculatedProfitEntity> GetCalculatedOrdersByEan(string ean)
        {
            return ReOrderCollection.Find(Query.EQ("Ean", ean)).ToList();
        }


        public BsonDocument GetSpecificBsonOrder(ObjectId id)
        {
            return ReOrderCollectionBson.FindOne(Query.EQ("_id", id));
        }
    }
}
