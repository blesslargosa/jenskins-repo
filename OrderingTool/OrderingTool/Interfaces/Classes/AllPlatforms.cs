﻿using DN_Classes.Entities;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.Interfaces.Classes
{
    public class AllPlatforms: IAllActions, IEbayAmazonActions
    {
       
        public Amazon Amazon { get; set; }
        public Ebay Ebay { get; set; }
        public ReOrdered ReOrdered { get; set; }

        public AllPlatforms(Amazon amazon, Ebay ebay, ReOrdered reordered) {

            this.Amazon = amazon;
            this.Ebay = ebay;
            this.ReOrdered = reordered;
        
        }

        public List<CalculatedProfitEntity> GetCalculatedOrders()
        {
            List<CalculatedProfitEntity> allOrders = new List<CalculatedProfitEntity>();

            var amazonOrders = Amazon.GetCalculatedOrders();
            var ebayOrders = Ebay.GetCalculatedOrders();
            var reOrderedItems = ReOrdered.GetCalculatedOrders(); 

            allOrders.AddRange(amazonOrders);
            allOrders.AddRange(ebayOrders);
            allOrders.AddRange(reOrderedItems);
            return allOrders;
        }

        public List<CalculatedProfitEntity> GetCalculatedOrdersPerSupplier(string suppliername)
        {
            List<CalculatedProfitEntity> allOrdersBySupplier = new List<CalculatedProfitEntity>();

            var amazonOrders = Amazon.GetCalculatedOrdersPerSupplier(suppliername);
            var ebayOrders = Ebay.GetCalculatedOrdersPerSupplier(suppliername);
            var reOrderedItems = ReOrdered.GetCalculatedOrdersPerSupplier(suppliername);

            allOrdersBySupplier.AddRange(amazonOrders);
            allOrdersBySupplier.AddRange(ebayOrders);
            allOrdersBySupplier.AddRange(reOrderedItems);

            return allOrdersBySupplier;
        }

        public string GetEanByOrderId(string orderId)
        {
            string ean = "";

            ean = Amazon.GetEanByOrderId(orderId);

            if (ean == "")
            {
                ean = Ebay.GetEanByOrderId(orderId);
            }
            return ean;
        }

        public List<string> GetMultipleEansByOrderId(string orderId)
        {
            //ebay ot implemented yet
            return Amazon.GetMultipleEansByOrderId(orderId);

           
        }
        public BsonDocument GetSpecificOrder(string ean, string supplier)
        {
            BsonDocument bson = new BsonDocument();
            bson = Amazon.GetSpecificOrder(ean, supplier);

            if (bson == null) {

                bson = Ebay.GetSpecificOrder(ean, supplier);

                if (bson == null) {
                    
                bson = ReOrdered.GetSpecificOrder(ean, supplier);
                }
            }

            return bson;

        }
       

        public ISingleAction GetOrderPlatform(string ean, string orderId)
        {
            return Amazon.GetOrderPlatform(ean, orderId) != null ? Amazon.GetOrderPlatform(ean, orderId) : Ebay.GetOrderPlatform(ean, orderId);
        }

        public List<CalculatedProfitEntity> GetSameOrders(string ean, string suppliername)
        {
            List<CalculatedProfitEntity> sameOrdersByEanSupplier = new List<CalculatedProfitEntity>();

            var amazonOrders = Amazon.GetSameOrders(ean,suppliername);
            var ebayOrders = Ebay.GetSameOrders(ean, suppliername);
            sameOrdersByEanSupplier.AddRange(amazonOrders);
            sameOrdersByEanSupplier.AddRange(ebayOrders);

            return sameOrdersByEanSupplier;
        }

        public List<string> GetAllOrderIds(GroupedItemsEntity order)
        {
            List<string> OrderIds = new List<string>();
            var amazongroupedOrders = Amazon.GetAllOrderIds(order);
            var ebaygroupedOrders = Ebay.GetAllOrderIds(order);
            var reorderGroupedOrders = ReOrdered.GetAllOrderIds(order);

            OrderIds.AddRange(amazongroupedOrders);
            OrderIds.AddRange(ebaygroupedOrders);
            OrderIds.AddRange(reorderGroupedOrders);

            return OrderIds;
        }


        public void UpdateReOrderedItem(string ean, string orderId, DateTime ReOrderDate, string note)
        {
            throw new NotImplementedException();
        }


        public List<CalculatedProfitEntity> GetCalculatedOrdersByEan(string ean)
        {
            List<CalculatedProfitEntity> allOrders = new List<CalculatedProfitEntity>();

            var amazonOrders = Amazon.GetCalculatedOrdersByEan(ean);
            var ebayOrders = Ebay.GetCalculatedOrdersByEan(ean);
            var reOrderedItems = ReOrdered.GetCalculatedOrdersByEan(ean);

            allOrders.AddRange(amazonOrders);
            allOrders.AddRange(ebayOrders);
            allOrders.AddRange(reOrderedItems);
            return allOrders;
        }


        public BsonDocument GetSpecificBsonOrder(ObjectId id)
        {
            BsonDocument bson = new BsonDocument();
            bson = Amazon.GetSpecificBsonOrder(id);

            if (bson == null)
            {

                bson = Ebay.GetSpecificBsonOrder(id);

                if (bson == null)
                {

                    bson = ReOrdered.GetSpecificBsonOrder(id);
                }
            }

            return bson;
        }


       
    }
}
