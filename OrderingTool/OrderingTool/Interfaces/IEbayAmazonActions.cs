﻿using DN_Classes.Entities;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.Interfaces
{
   public interface IEbayAmazonActions
    {
       // methods intended for Ebay and Amazon
        String GetEanByOrderId(string orderId);
        ISingleAction GetOrderPlatform(string ean, string orderId);
        List<CalculatedProfitEntity> GetSameOrders(string ean, string suppliername);
        List<string> GetMultipleEansByOrderId(string orderId);
    }
}
