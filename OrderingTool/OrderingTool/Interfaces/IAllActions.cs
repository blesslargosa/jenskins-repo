﻿using DN_Classes.Entities;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.Interfaces
{
   public interface IAllActions
    {
        // methods intended for Ebay, Amazon and ReOrders
       List<CalculatedProfitEntity> GetCalculatedOrders();
       List<CalculatedProfitEntity> GetCalculatedOrdersPerSupplier(string suppliername);
       List<string> GetAllOrderIds(GroupedItemsEntity order);
       BsonDocument GetSpecificOrder(string ean, string supplier);
       BsonDocument GetSpecificBsonOrder(ObjectId id);
       List<CalculatedProfitEntity> GetCalculatedOrdersByEan(string ean);
    }
}
