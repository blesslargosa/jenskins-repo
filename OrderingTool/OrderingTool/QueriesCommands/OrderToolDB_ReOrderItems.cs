﻿using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.QueriesCommands
{
    public class OrderToolDB_ReOrderItems : DBCollections
    {
        public bool IsItemReOrdered(string orderId)
        {

            bool hasReOrdered = false;
            var orderedItems = ReOrderCollection.Find(Query.EQ("AmazonOrder.AmazonOrderId", orderId)).ToList();

            if (orderedItems.Count == 1)
            {
                hasReOrdered = true;

            }
            return hasReOrdered;
        }


       
    }
}
