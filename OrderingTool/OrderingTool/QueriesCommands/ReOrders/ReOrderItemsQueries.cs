﻿
using DN_Classes.Entities;
using MongoDB.Driver.Builders;
using OrderingTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.QueriesCommands.ReOrders
{
    public class ReOrderItemsQueries : DBCollections
    {
        public List<CalculatedProfitEntity> GetReOrders() {

           return ReOrderCollection.FindAll().ToList();
        
        }

        public List<CalculatedProfitEntity> GetItemsByOrderId(string orderId)
        {

            return ReOrderCollection.Find(Query.EQ("AmazonOrder.AmazonOrderId", orderId)).ToList();
        
        }
    }
}
