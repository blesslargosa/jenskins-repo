﻿
using AmazonOrders;
using DN_Classes.Entities;
using DN_Classes.Products.Price;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using OrderingTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool.QueriesCommands.ReOrders
{
    public class ReOrderItemsCommands : DBCollections
    {

        public void ImportReOrderItem(CalculatedProfitEntity item)
        {
            GenerateId generate = new GenerateId();
            item._id = generate.GenerateObjectId();
            ReOrderCollection.Insert(item);


        }

        public ObjectId ImportReOrderItemByEan(string ean, BuyPriceRow row, DateTime date)
        {
            GenerateId generate = new GenerateId();
            ObjectId newId = generate.GenerateObjectId();
            ItemEntity item = new ItemEntity
            {
                NewBuyPrice = row.price
            };

            CalculatedProfitEntity newItem = new CalculatedProfitEntity
            {
                _id = newId,
                Ean = ean,
                SupplierName = row.suppliername,
                ArticleNumber = row.artnr,
                Item = item,
                VE = row.packingUnit,
                PurchasedDate = date
            };

            ReOrderCollection.Insert(newItem);

            return newId;
        }

        public void UpdateReOrderItemByEan(ObjectId id, BuyPriceRow row, string note, DateTime reOrderDate)
        {
            ReOrderCollection.Update(Query.And(Query.EQ("_id", id), Query.EQ("SupplierName", row.suppliername)),
            Update.Set("plus.reOrderDate", reOrderDate)
            .Set("plus.note", note));
        }

        public void DeleteReOrderedItem(ObjectId id)
        {

            ReOrderCollection.Remove(Query.EQ("_id", id));

        }
    }
}
