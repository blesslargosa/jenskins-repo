﻿using DN_Classes.Entities;
using DN_Classes.Supplier;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using OrderingTool.Entities;
using OrderingTool.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderingTool
{
    public class OrderToolDB_GroupedItems : DBCollections
    {

     
        /// <summary>
        /// this will get all exported/ ordered items
        /// </summary>
        /// <returns>List<GroupedItemsEntity></returns>
        public List<GroupedItemsEntity> GetExportedOrders()
        {
            try
            {
                   return GroupedItems.FindAll().ToList();
              

            }
            catch (Exception msg) { }

            return null;
        }

       

        /// <summary>
        /// save single generated item with datetime as ID
        /// </summary>
        /// <param name="groupedItemsEntity"></param>
        /// <param name="dateTick"></param>
        public void SaveOrderedItems(GroupedItemsEntity groupedItemsEntity, String dateTick)
        {
            groupedItemsEntity.supplierOrderId = groupedItemsEntity.supplier + "_" + dateTick;
            GroupedItems.Insert(groupedItemsEntity);
        }

        /// <summary>
        /// get grouped orders by supplier
        /// </summary>
        /// <param name="supplier"></param>
        /// <returns>List<GroupedItemsEntity></returns>
        public List<GroupedItemsEntity> GetGeneratedGroupedItems(string supplier)
        {

            return GroupedItems.Find(Query.EQ("supplier", supplier)).ToList();

        }

        /// <summary>
        /// get grouped orders by supplierOrderId
        /// </summary>
        /// <param name="supplierOrderID"></param>
        /// <returns>List<GroupedItemsEntity></returns>
        public List<GroupedItemsEntity> GetOrdersBySupplierOrderId(string supplierOrderId)
        {

            return GroupedItems.Find(Query.EQ("supplierOrderId", supplierOrderId)).ToList();

        }

        





       
    }
}