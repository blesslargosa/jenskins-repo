﻿
using DN_Classes.Entities;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderingTool
{
    public class OrderToolDB_UnOrderedItems : DBCollections
     {
         /// <summary>
         /// this will get all unordered calculated items
         /// </summary>
         /// <returns></returns>
         public List<CalculatedProfitEntity> GetCalculatedOrders()
         {
             List<CalculatedProfitEntity> allOrders = new List<CalculatedProfitEntity>();
             try
             {
                 var amazonOrders = Collection.Find(Query.And
                     (Query.NotExists("plus.supplierOrderId"), 
                      Query.Or(Query.NotExists("plus.excludeFromOrdering"),Query.EQ("plus.excludeFromOrdering",false)))).ToList();

                 var ebayOrders = EbayCalculatedCollection.Find(Query.And(Query.NotExists("plus.supplierOrderId"), Query.NotExists("plus.excludeFromOrdering"))).ToList();
                 var reOrders = ReOrderCollection.Find(Query.And(Query.NotExists("plus.supplierOrderId"), Query.NotExists("plus.excludeFromOrdering"))).ToList();

                
                 allOrders.AddRange(amazonOrders);
                 allOrders.AddRange(ebayOrders);
                 allOrders.AddRange(reOrders);

                 return allOrders;
             }
             catch (Exception msg) { }

             return null;
         }

         /// <summary>
         /// will update single item in the singlecalculateitem collection by putting orderedDate
         /// </summary>
         /// <param name="objectIds"></param>
         /// <param name="supplier"></param>
         /// <param name="dateTick"></param>
         public void UpdateCalculatedOrder(List<ObjectId> objectIds, string supplier, string dateTick)
         {
             foreach (var objectid in objectIds)
             {
                 string ob2 = objectid.ToString().Replace("{", "").Replace("}", "");
                 try
                 {
                    
                     Collection.Update(Query.EQ("_id", ObjectId.Parse(ob2)),
                         Update.Set("plus.supplierOrderId", supplier + "_" + dateTick)
                         .Set("plus.ordered", true).Set("plus.excludeFromOrdering", true));

                     EbayCalculatedCollection.Update(Query.EQ("_id", ObjectId.Parse(ob2)),
                      Update.Set("plus.supplierOrderId", supplier + "_" + dateTick)
                      .Set("plus.ordered", true).Set("plus.excludeFromOrdering", true));

                    
                     ReOrderCollection.Update(Query.EQ("_id", ObjectId.Parse(ob2)),
                     Update.Set("plus.supplierOrderId", supplier + "_" + dateTick)
                     .Set("plus.ordered", true).Set("plus.excludeFromOrdering", true));
                 
                 
                 }
                 catch {
                     
                 
                 }
                 
             }
         }

         /// <summary>
         /// this will get all orders per supplier
         /// </summary>
         /// <param name="suppliername"></param>
         /// <returns>List<CalculatedProfitEntity></returns>
         public List<CalculatedProfitEntity> GetCalculatedOrdersPerSupplier(string suppliername)
         {
             return Collection.Find(Query.EQ("SupplierName", suppliername)).ToList();
         }

         /// <summary>
         /// this will get specific order from calculatedOrders
         /// </summary>
         /// <param name="ean"></param>
         /// <param name="suppliername"></param>
         /// <returns>BsonDocument</returns>
         public BsonDocument GetSpecificOrder(string ean, string supplier)
         {

             return AmazonCollectionBson.FindOne(Query.And(Query.EQ("SupplierName", supplier), Query.EQ("Ean", ean)));

         }

         /// <summary>
         /// this will reupdate the calculatedOrders to ordered items
         /// </summary>
         /// <param name="AmazonOrderId"></param>
         /// <param name="supplierOrderId"></param>
         /// <param name="ean"></param>
         /// <returns></returns>
         public void ReUpdateCalculatedOrder(string OrderId, string supplierOrderId, string ean, string objectid)
         {
             if (OrderId != null)
             {
                 // string ob2 = objectid.ToString().Replace("{", "").Replace("}", "");
                 Collection.Update(Query.And(Query.EQ("AmazonOrder.AmazonOrderId", OrderId), Query.EQ("Ean", ean)), Update.Set("plus.supplierOrderId", supplierOrderId).Set("plus.ordered", true));
                 //  Collection.Update(Query.And(Query.EQ("EbayOrder.OrderId", OrderId), Query.EQ("Ean", ean)), Update.Set("plus.supplierOrderId", supplierOrderId).Set("plus.ordered", true));
             }
             else {



                 Collection.Update(Query.And(Query.EQ("_id", ObjectId.Parse(objectid)), Query.EQ("Ean", ean)), Update.Set("plus.supplierOrderId", supplierOrderId).Set("plus.ordered", true));
                
             
             }

         }

        

     }



}
