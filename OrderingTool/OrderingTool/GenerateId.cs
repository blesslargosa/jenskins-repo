﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmazonOrders
{
    public class GenerateId
    {
        private static readonly Random _random = new Random();

        public ObjectId GenerateObjectId()
        {
            var timestamp = DateTime.UtcNow;
            var machine = _random.Next(0, 16777215);
            var pid = (short)_random.Next(0, 32768);
            var increment = _random.Next(0, 16777215);

            return new ObjectId(timestamp, machine, pid, increment);
        }
    }
}